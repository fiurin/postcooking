import os

PROJECT_NAME = 'Postcooking'

CONTENT_ROOT = 'content'
OUTPUT_ROOT = 'output'

THEME_PATHS = [
    'theme',
]

PLUGINS = [
    'flamingo.plugins.Menu',
    'flamingo.plugins.Bootstrap4',
    'flamingo.plugins.jQuery3',

    # Plugin for ReipeMD
    # https://recipemd.org/specification.html
    # This Plugin assumes, that all *.md files are recipe.md files
    'flamingo_recipemd.RecipeMd',

    # Plugin for RSS Feed
    'flamingo.plugins.Feeds',
    # Git Plugin, copied from blog.tinyhost.de
     'plugins/git.py::Git',
]

POST_BUILD_LAYERS = [
    'overlay',
]

COUNTRIES = {
    'AT': {'name': 'Austria', 'flag': '🇦🇹', },
    'BE': {'name': 'Belgium', 'flag': '🇧🇪', },
    'BY': {'name': 'Belarus', 'flag': '🇧🇾', },
    'CA': {'name': 'Canada', 'flag': '🇨🇦', },
    'CN': {'name': 'China', 'flag': '🇨🇳', },
    'CZ': {'name': 'Czechia', 'flag': '🇨🇿', },
    'ES': {'name': 'Estonia', 'flag': '🇪🇪', },
    'FI': {'name': 'Finland', 'flag': '🇫🇮', },
    'FR': {'name': 'France', 'flag': '🇫🇷', },
    'GB': {'name': 'Great Britain', 'flag':'🇬🇧',},
    'HU': {'name': 'Hungary', 'flag': '🇭🇺', },
    'IN': {'name': 'India', 'flag': '🇮🇳', },
    'IT': {'name': 'Italy', 'flag': '🇮🇹', },
    'JP': {'name': 'Japan', 'flag': '🇯🇵', },
    'LT': {'name': 'Lithuania', 'flag': '🇱🇹', },
    'LV': {'name': 'Latvia', 'flag': '🇱🇻', },
    'NL': {'name': 'The Netherlands', 'flag': '🇳🇱'},
    'NO': {'name': 'Norway', 'flag': '🇳🇴', },
    'NZ': {'name': 'New Zeeland', 'flag': '🇳🇿', },
    'PL': {'name': 'Poland', 'flag': '🇵🇱',},
    'RU': {'name': 'Russia', 'flag': '🇷🇺', },
    'SE': {'name': 'Sweden', 'flag': '🇸🇪', },
    'TW': {'name': 'Taiwan', 'flag': '🇹🇼', },
    'US': {'name': 'United States of America', 'flag': '🇺🇸', },
}
FLAVOUR = {
    'drink': '<i class="fa-solid fa-martini-glass"></i>',
    'savoury': '<i class="fa-solid fa-bowl-food"></i>',
    'sweet': '<i class="fa-solid fa-candy-cane"></i>',
}
FEEDS_DOMAIN = 'https://postcooking.fiurin.de'
FEEDS = [
    {
        'id': 'postcooking.fiurin.de/feed',
        'title': 'postcooking',
        'type': 'atom',
        'output': 'all.atom.xml',
        'contents':
            lambda ctx: ctx.contents.filter(
                path__startswith="postcrossing-recipes/"
            ).order_by(
                'date',
            ),
        'entry-id':
            lambda content: 'tag:postcooking.fiurin.de/{}'.format(
                os.path.basename(content['output']),
            ),
        'updated':
            lambda content: content['gitupdate'].isoformat(),
        'published':
            lambda content: content['date'].isoformat(),
    },
]
