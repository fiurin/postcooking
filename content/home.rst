id: home
title: Postcooking
output: index.html
template: home.html


Postcooking
===========

On this website I collect recipes, from all over the world,
which I have received via the fantastic `postcrossing <https://postcrossing.com>`_
project.

Enjoy your meal!


