# Zimtsterne

*CZ, sweet, baked, vegetarian*

---

- *2* Eier
- *130 g* Puderzucker
- *380 g* Mehl
- *250 g* Butter
- *1 TL* Zimt
- Kristallzucker, gemahlene Nüsse nach Belieben

---

Mehl, Puderzucker und Zimt gut vermischen.
Weiche Butter und 1 Ei hinzufügen und zu
einem glatten Teig kneten.

Den Teig auf einem bemehlten Brett dünn ausrollen.
Mit einer Form Sterne ausstechen und die Plätzchen
auf ein mit Backpapier ausgelegtes Backblech legen.

Das zweite Ei aufschlagen und jedes Plätzchen damit
bestreichen. Diese nun mit Zucker oder den Nüssen
bestreuen.

Im vorgeheizten Backofen bei 180°C - 200°C ca 10
Minuten backen.
