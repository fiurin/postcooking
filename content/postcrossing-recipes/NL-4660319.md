# Hutspot

*NL, savoury, cooked*

---

- *1 kg* peeled potatoes 
- *1 kg* chopped carrots 
- *500 g* chopped onions 
- a little milk 
- salt and pepper
- 1 smoked sausage

---

Put the potatoes, the carrots and the onions in a pan.
Cook for about 30 minutes, then mash it with a masher.
Add a little milk to make it creamy.
Flavor with salt and pepper.
Serve with a smoked sausage.
