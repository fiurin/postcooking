# Kutia

*BY, sweet, cooked, vegetarian*

---

- *1 cup* wheat berries 
- *100 g* raisins 
- *200 g* dried fruit and nuts 
- *2 tsp* honey 
- *1 pinch* of salt 

---

Soak wheat berries in water for 2-3 hours.
Cook them in salted water.
Soak raisins in hot water for 5-10 minutes.
Cut dried fruits.
Mix wheat berries, dried fruits, raisins and honey.
Serve it cold.
