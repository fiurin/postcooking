# Greek Orzo

*US, savoury, cooked*

---

- Orzo (greek pasta) 
- beef, chicken bullion
- butter

---

Boil water for 15 minutes with beef and chicken bullion,
then add the orzo in a drainer and keep stirring for about 20 minutes.

Add butter, when served.
