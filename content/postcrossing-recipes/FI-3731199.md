# Sautéed reindeer from North

*FI, savoury, fried*

---

- *500 g* reindeer meat 
- *3 tbsp* butter 
- *2 dl* water 
- *1 tsp* salt 
- *1 tsp* black pepper 
- *2 tbsp* wheat flour 

---

Use frozen meat, let it slightly thaw so you can cut it
into very thin slices.
Melt the butter in the pan and add the meat in small amounts.
Sauté on high heat and set aside once browned.
Put all meat finally back to pan, add water and salt
(and pepper if you want).
Let it simmer in low temperature under a lid for 1-2 hours
until meat is tender, add water if necessary.

If you want thicker both, you can mix cold water and flour
and add it to the pan at the end of cooking.

Other meat, such as moose or deer, can also be used for
the recipe.
Enjoy with mashed potatoes and lingonberry jam.
