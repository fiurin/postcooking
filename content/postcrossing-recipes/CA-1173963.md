# Riverbank Casserole

*CA, savoury, baked, vegetarian*

---

- *4 - 5* medium potatoes, cooked
- *75 ml* sour cream  
- *1 tsp* salt 
- *1* dash of pepper 
- *2 tsp* of chopped chives 
- *1/8 tsp* dill seed  
- *250 ml* of cooked, well drained spinach - finely chopped 
- *125 ml* of grated cheese 

---

Pre-heat oven to 400°F. Mash potatoes well.
Add all of the above ingredients except cheese & mix well.
Put in a 30 cm x 30 cm greased casserole.
Sprinkle grated cheese on top.
Cook for 20 mins. in oven.

If you like add some seafood, such as shrimps, scallops or
salmon and season with lemon juice.
