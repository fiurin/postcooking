import datetime
import logging
import os
import subprocess

logger = logging.getLogger("project.git")


class Git:
    def contents_parsed(self, context):
        now = datetime.datetime.now().astimezone(datetime.timezone.utc)
        state = subprocess.getoutput(["git log -1 --pretty=format:'%H %aI'"])
        logger.debug("Current state for the whole project is %s", state)
        for content in context.contents:
            content["gitstate"] = state

            # get a list of update timestamps for this content
            # run 'git' in the directory of the content to account for git submodules.
            fullpath = os.path.join("content", content["path"])
            dirname, filename = os.path.split(fullpath)
            try:
                updates = subprocess.check_output(
                    "git log --pretty=format:'%aI' {}".format(filename),
                    shell=True,
                    cwd=dirname,
                )
                updates = updates.decode()
                updates = updates.split('\n')
                if ''.join(updates).strip():
                    created = datetime.datetime.fromisoformat(updates[-1])
                    updated = datetime.datetime.fromisoformat(updates[0])
                else:
                    created = now
                    updated = now

                # if the article has a 'date' we will override it from git
                content['date'] = created

                content['gitupdate'] = updated

                continue
            except subprocess.CalledProcessError:
                pass
            except IndexError:
                pass

            # fallback action: This file is unknown to git
            logger.info("Falling back to article date %s for %s", content["gitupdate"], fullpath)
            mtime = datetime.datetime.fromtimestamp(
                os.path.getmtime(
                    os.path.join(context.settings.CONTENT_ROOT, content["path"])
                    )
            ).replace(tzinfo=datetime.timezone.utc)
            content['gitupdate'] = mtime

            if "date" not in content:
                content["date"] = mtime
            else:
                # if there is a date in the content we will assume it's utc (even if that is 100% wrong)
                # but since it's only a date and we want to have a datetime there is no real point in caring about the
                # timezone
                date = datetime.datetime.strptime(content['date'], '%Y-%m-%d')
                date.replace(tzinfo=datetime.timezone.utc)
                content['date'] = date

